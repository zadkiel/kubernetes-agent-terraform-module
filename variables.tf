variable "annotations" {
    type = map
}

variable "gitlab_project_id" {
  type = string
}

variable "agent_name" {
  type = string
  default = "my-kubernetes-agent"
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "kas_address" {
  type = string
  default = "wss://kas.gitlab.com"
  description = "The wss URL of your Gitlab Kubernetes Agent Server"
}
variable "agent_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "agent_version" {
  type = string
  default  = "stable"
}
